package cmd

import (
	"encoding/hex"
	"fmt"
	"log"
	"os"
)

type Color struct {
	Hex    string
	RGB    RGB
	String string
}

type RGB struct {
	R int
	G int
	B int
}

func (r RGB) PrintCSV() string {
	return fmt.Sprintf("%d,%d,%d", r.R, r.G, r.B)
}

func (c Color) Lighten(a float64) Color {
	n := c
	err := n.SetHex(modifyColor(n.Hex, a))
	if err != nil {
		log.Fatal(err)
	}
	_ = n.GenRGB()

	return n
}

func (c Color) Darken(a float64) Color {
	n := c
	err := n.SetHex(modifyColor(n.Hex, -a))
	if err != nil {
		log.Fatal(err)
	}
	_ = n.GenRGB()

	return n
}

func (c *Color) SetHex(s string) error {
	stripped, err := strip(s)
	if err != nil {
		return fmt.Errorf("parsing error for hex color: %s", err.Error())
	}

	dec, err := hex.DecodeString(stripped)
	if err != nil {
		return fmt.Errorf("decode error: %s", err.Error())
	}

	if len(dec) != 3 {
		return fmt.Errorf("decoded string (\"%s\") is wrong length: %v", c.Hex, dec)
	}

	c.Hex = stripped
	return nil
}

func (c *Color) GenRGB() error {
	decodedBytes, err := hex.DecodeString(c.Hex)
	if err != nil {
		return fmt.Errorf("decode error: %s", err.Error())
	}

	if len(decodedBytes) != 3 {
		return fmt.Errorf("decoded string (\"%s\") is wrong length: %v", c.Hex, decodedBytes)
	}

	c.RGB = RGB{
		R: int(decodedBytes[0]),
		G: int(decodedBytes[1]),
		B: int(decodedBytes[2]),
	}

	return nil
}

func getColorscheme(configMap map[string]string) map[string]Color {
	if configMap == nil {
		log.Fatal("Unable to pull colorscheme from config")
	}

	colorMap := make(map[string]Color, 1)

	for k, v := range configMap {
		if k == "wallpaper" {
			colorMap[k] = Color{
				String: v,
			}
			continue
		}
		var c Color

		err := c.SetHex(v)
		fatalErr(err)

		err = c.GenRGB()
		fatalErr(err)

		colorMap[k] = c
	}

	colorMap["user"] = Color{
		String: os.Getenv("USER"),
	}

	return colorMap
}
