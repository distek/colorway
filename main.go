package main

import (
	"log"

	"codeberg.org/distek/colorway/cmd"
)

func main() {
	log.SetFlags(log.Lshortfile | log.LstdFlags)
	cmd.Execute()
}
