package cmd

import (
	"encoding/hex"
	"fmt"
	"log"
	"math"
)

// Strip hex color from various formatting types down to it's simple form
// #123456, 0x123456, etc -> 123456
func strip(h string) (string, error) {
	switch len(h) {
	case 0:
		return "", fmt.Errorf("input color is of length 0")
	case 3:
		var retString string

		for _, v := range h {
			retString += string(v)
			retString += string(v)
		}

		return retString, nil
	case 4:
		if h[0] == '#' {
			return h[1:], nil
		}
		return "", fmt.Errorf("input color not of correct format: %s", h)
	case 5:
		return "", fmt.Errorf("input color not of correct format: %s", h)
	case 6:
		if h[0] == '#' {
			return "", fmt.Errorf("input color not of correct format: %s", h)
		}
		return h, nil
	case 7:
		if h[0] == '#' {
			return h[1:], nil
		}

		if h[0] == 'x' {
			return h[1:], nil
		}

		return "", fmt.Errorf("input color not of correct format: %s", h)
	case 8:
		if h[0:1] == "0x" || h[0:1] == "0X" {
			return h[2:], nil
		}

		return "", fmt.Errorf("input color not of correct format: %s", h)
	}

	return "", fmt.Errorf("input color not of correct format: %s", h)
}

func modifyColor(c string, a float64) string {
	decodedBytes, err := hex.DecodeString(c)
	if err != nil {
		log.Fatal(fmt.Errorf("decode error: %s", err.Error()))
	}

	if len(decodedBytes) != 3 {
		log.Fatal(fmt.Errorf("decoded string (\"%s\") is wrong length: %v", c, decodedBytes))
	}

	modSlice := make([]string, 3)

	for i := 0; i < 3; i++ {
		v := int(decodedBytes[i]) + int(math.Floor(float64(decodedBytes[i])*(a*100/255)))

		if v > 255 {
			v = 255
		} else if v < 0 {
			v = 0
		}

		modSlice[i] = hex.EncodeToString([]byte{byte(v)})
	}

	return fmt.Sprintf("%s%s%s", modSlice[0], modSlice[1], modSlice[2])
}

func fatalErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
