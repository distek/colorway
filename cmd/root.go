package cmd

import (
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"

	"github.com/spf13/viper"
)

var (
	cfgDir string
)

var rootCmd = &cobra.Command{
	Use: "colorway",
	Run: func(cmd *cobra.Command, args []string) {
		_ = cmd.Help()
	},
}

func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	rootCmd.PersistentFlags().StringVar(&cfgDir, "config-dir", "", "config directory (default is $HOME/.config/colorway)")

	cobra.OnInitialize(initConfig)
}

func initConfig() {
	setHome()
	cachePath = home + "/.cache/colorway"

	if cfgDir != "" {
		cfgDir = strings.TrimSuffix(cfgDir, "/")

		viper.SetConfigFile(cfgDir + "/" + "config.yaml")
	} else {
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		viper.AddConfigPath(home + "/.config/colorway")
		viper.SetConfigType("yaml")
		viper.SetConfigName("config")

		cfgDir = home + "/.config/colorway"
	}

	configPath = cfgDir
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		log.Println(err)
	}
}
