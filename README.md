# colorway


## Installation

Download release binary and place it somewhere in `$PATH`
Use the `example-conf` directory in this repo as an example on how to structure your config in `$HOME/.config`.
Or just clone this repo and snag that one.

## Usage

The config directory can be specified by using `--config-dir /path/to/config`, otherwise it will use `$HOME/.config`.

Template files are setup like so:

```
@define-color background rgba({{.background.RGB.R}}, {{.background.RGB.G}}, {{.background.RGB.B}}, 0.7);
@define-color foreground #{{.foreground.Hex}};
@define-color color0 #{{.color0.Hex}};
@define-color colorSomethingElse #{{.colorSomethingElse.Hex}};
...
```

Specify the color name and value in the config (the name can be customized to your liking):

```yaml
colorscheme:
  background: "#090618"
  foreground: "#dcd7ba"
  color0: "#090618"
  colorSomethingElse: "#090618"
```

Then, specify the color type with `.Hex`, `.RGB.<R|G|B>`, or `.RGB.PrintCSV`, which will replace the value with what you'd expect:

- HEX comes out as `123456`, note without the `#`
- RGB is a struct of R, G, and B, can be used like so:
  - `@define-color background rgba({{.background.RGB.R}}, {{.background.RGB.G}}, {{.background.RGB.B}}, 0.7);
`
Also, in the config, you can now specify `post-exec` which is a slice of commands to run after the generation has completed:

```yaml
post-exec:
  - "oomox-cli ~/.cache/colorway/colors.oomox -m all"
  - "oomox-gnome-colors-icons-cli ~/.config/oomox/colors/colors.oomox"
```

## Advanced? Usage

You can use the `Lighten` or `Darken` methods to modify the colors on the fly in the tempalte:
```
{{$nb := .color3.Darken 1.3}}{{$nb.RGB.PrintCSV}}
```

Go templating doesn't appear to allow chaining of methods in any other way beyond this, so sorry about the syntax.

## Why not pywal

You have the colorscheme you like already, just want a way to propagate it amongst all the various programs that allow color changes.

And also this is written in Go, so it's _BLAZINGLY FAST_
