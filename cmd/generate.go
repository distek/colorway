package cmd

import (
	"fmt"
	"io/fs"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"text/template"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	home       string
	cachePath  string
	configPath string
)

func setHome() {
	var err error
	home, err = os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}
}

func generateInit() {
	err := os.MkdirAll(home+"/.cache/colorway", 0700)
	if err != nil {
		if !os.IsExist(err) {
			log.Fatal("mkdir error:", err)
		}
	}
}

var generateCmd = &cobra.Command{
	Use: "generate",
	Run: func(cmd *cobra.Command, args []string) {
		generateInit()

		colorScheme := getColorscheme(viper.GetStringMapString("colorscheme"))

		err := filepath.Walk(configPath+"/templates/", func(path string, info fs.FileInfo, err error) error {
			if err != nil {
				log.Fatal(err)
			}

			if info.IsDir() {
				return nil
			}

			f, err := os.ReadFile(path)
			if err != nil {
				log.Fatal(err)
			}

			t := template.New(info.Name())

			t, err = t.Parse(string(f))
			if err != nil {
				log.Fatal(err)
			}

			newName := strings.TrimSuffix(info.Name(), ".tmpl")
			newSplit := strings.Split(newName, "-")

			var outputFile string
			if len(newSplit) > 1 {
				outputFile = cachePath + "/colors-" + newSplit[0] + "." + newSplit[1]
			} else {
				// I don't have the effort in me to logic out ever scenario right now
				outputFile = cachePath + "/colors." + newName
			}

			fo, err := os.Create(outputFile)
			if err != nil {
				return err
			}

			err = t.Execute(fo, colorScheme)
			if err != nil {
				return err
			}

			return nil
		})

		if err != nil {
			log.Fatal(err)
		}

		for _, v := range viper.GetStringSlice("post-exec") {
			fmt.Printf("Running: %s: ", v)
			output, err := exec.Command("sh", "-c", v).CombinedOutput()
			if err != nil {
				fmt.Printf("\033[31mfailed\033[0m\n")
				log.Println("Error running:", v)
				log.Println("Output:")
				fmt.Println(string(output))
			} else {
				fmt.Printf("\033[32msuccess\033[0m\n")
			}
		}
	},
}

func init() {
	rootCmd.AddCommand(generateCmd)
}
